# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository was created to keep presentation materials that will be developed and used by Marcin Ufniarz.  
This repository is here to encapsolate all of my presentations and lecture matterials.

I imagine it will also be used for other informational material that isn't related to any presentations, lectures or talks.

### How do I get set up? ###

`git clone https://mufniarz@bitbucket.org/mufniarz/mu-presentations-at-magnet.git <directory name (optional)>`

### Contribution guidelines ###

Anyone within Magnet 360 or Mindtree can request access and contribute.

### Who do I talk to? ###

Please reach out to Marcin Ufniarz for any questions.

Updating for a git fetch test.